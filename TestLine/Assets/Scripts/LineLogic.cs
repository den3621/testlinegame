﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class LineLogic : MonoBehaviour
{
    const int TIME_STEP = 3;

    public float timeToDraw = 20;
    public Text timerText;
    public List<GameObject> figList = new List<GameObject>();
    public Text congText;

    LineRenderer _line = null;
    Vector3 _startPos, _endPos;
    bool _canDraw = true;
    GameObject _figure;
    List<Vector3> _linePoints = new List<Vector3>();
    float _canvDist;

    void OnEnable()
    {
        CheckStep.MyEvent += refreshScore;
    }

    void OnDisable()
    {
        CheckStep.MyEvent -= refreshScore;
    }

    void Start()
    {
        _canvDist = GameObject.FindObjectOfType<Canvas>().planeDistance;
        GameObject[] figures = Resources.LoadAll<GameObject>("Figures");
        for (int i = 0; i < figures.Length; i++)
            figList.Add(figures[i]);
        RandomFigure();
        StartCoroutine(TimeToDraw(timeToDraw));
    }

    void RandomFigure()
    {
        int rand = Random.Range(0, figList.Count);
        _figure = Instantiate(figList[rand]);
        RectTransform figureRect = _figure.GetComponent<RectTransform>();
        figureRect.SetParent(FindObjectOfType<Canvas>().GetComponent<RectTransform>());
        figureRect.localPosition = new Vector3(0, 0, 10);
        figureRect.localScale = Vector3.one;
        figList.RemoveAt(rand);
    }

    IEnumerator TimeToDraw(float myTime)
    {
        while (myTime > 0)
        {
            timerText.text = myTime.ToString();
            myTime--;
            yield return new WaitForSeconds(1);
        }
        MainMenu mm = FindObjectOfType<MainMenu>();
        mm.restartB.gameObject.SetActive(true);
        mm.scoreText.text = "Score: " + MainMenu.score.ToString();
        CheckStep.step = 0;
        Destroy(_figure);
        Destroy(gameObject);
    }

    public void OnDown()
    {
        if (_canDraw)
        {
            _startPos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, _canvDist));
            if (_line == null)
            {
                _line = new GameObject("Line").AddComponent<LineRenderer>();
                _line.tag = "MyLine";
                _line.GetComponent<Renderer>().material = Resources.Load<Material>("lineMaterial");
                Transform lineTrans = _line.GetComponent<Transform>();
                lineTrans.SetParent(transform);
                lineTrans.localScale = Vector3.one;
                lineTrans.localPosition = new Vector3(lineTrans.localPosition.x, lineTrans.localPosition.y, 0);
                _line.SetWidth(.1f, .1f);
            }
        }
    }

    public void OnUp()
    {
        if (_canDraw)
            StartCoroutine(LineTrail(0.04f));
        _canDraw = false;
    }

    public void OnDrag()
    {
        if (_canDraw)
        {
            _endPos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, _canvDist));
            if (_endPos != _startPos)
            {
                _linePoints.Add(_startPos);
                Vector3[] vec = new Vector3[_linePoints.Count];
                _linePoints.CopyTo(vec);
                _line.SetVertexCount(vec.Length);
                _line.SetPositions(vec);
                RaycastHit2D rhit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
                if (rhit.collider != null)
                    rhit.collider.GetComponent<CheckStep>().Checking();
                _startPos = _endPos;
            }
        }
    }

    IEnumerator LineTrail(float myTime)
    {
        Color myColor = new Color(1, 1, 1, 0.5f);
        Material mat = _line.material;
        while (myColor.a > 0)
        {
            myColor.a -= myTime;
            mat.SetColor("_TintColor", myColor);
            yield return new WaitForSeconds(myTime);
        }
        Destroy(GameObject.FindGameObjectWithTag("MyLine"));
        mat.SetColor("_TintColor", Color.red);
        CheckStep.step = 0;
        CheckStep[] checkPoints = FindObjectsOfType<CheckStep>();
        for (int i = 0; i < checkPoints.Length; i++)
            checkPoints[i].oneTimeStep = false;
        _linePoints.Clear();   
        _line = null;
        _canDraw = true;
    }

    IEnumerator Congratulation()
    {
        congText.gameObject.SetActive(!congText.gameObject.activeInHierarchy);
        yield return new WaitForSeconds(1.5f);
        congText.gameObject.SetActive(!congText.gameObject.activeInHierarchy);
        if (figList.Count > 0)
        {
            RandomFigure();
            timeToDraw -= TIME_STEP;
            StartCoroutine(TimeToDraw(timeToDraw));
        }
        else
            UnityEngine.SceneManagement.SceneManager.LoadScene("scen1");
    }

    void refreshScore(int val)
    {
        MainMenu mm = FindObjectOfType<MainMenu>();
        mm.scoreText.text = "Score: " + val.ToString();
        Destroy(FindObjectOfType<MyFigure>().gameObject);
        StopAllCoroutines();
        StartCoroutine(Congratulation());
    }
}
