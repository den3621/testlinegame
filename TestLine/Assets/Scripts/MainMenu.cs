﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public Button startB, restartB;
    public Text scoreText;
    public static int score;

    void Start()
    {
        Resources.Load<Material>("lineMaterial").SetColor("_TintColor", Color.red);
    }

    public void StartGame (GameObject myBut)
    {
        score = 0;
        scoreText.text = score.ToString();
        GameObject firstLevel = Instantiate(Resources.Load<GameObject>("Level1Figures"));
        RectTransform firstLevelRect = firstLevel.GetComponent<RectTransform>();
        firstLevelRect.SetParent(GetComponent<RectTransform>());
        firstLevelRect.localScale = Vector3.one;
        firstLevelRect.offsetMin = Vector2.zero;
        firstLevelRect.offsetMax = Vector2.zero;
        firstLevelRect.localPosition = Vector3.zero;
        if (!scoreText.gameObject.activeInHierarchy)
            scoreText.gameObject.SetActive(true);
        if(myBut.name != "StartB")
            myBut.SetActive(false);
        else
            Destroy(myBut);
    }
}
