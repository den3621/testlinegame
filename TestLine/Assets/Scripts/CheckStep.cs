﻿using UnityEngine;
using System.Collections;

public class CheckStep : MonoBehaviour
{
    public bool oneTimeStep = false;
    public static int step;
    public int priority;
    public delegate void Deleg(int val);
    public static event Deleg MyEvent;

    private bool _check = false;

    public void Checking ()
    {
        if (!oneTimeStep)
        {
            oneTimeStep = true;
            if (step >= priority)
                step++;
        }
        else if (!_check && priority == 0 && step == GetComponentInParent<MyFigure>().checkPointsCount)
        {
            _check = true;
            MainMenu.score++;
            MyEvent(MainMenu.score);
        }
    }
}
